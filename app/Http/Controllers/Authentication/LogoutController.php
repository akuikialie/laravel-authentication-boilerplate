<?php

namespace App\Http\Controllers\Panel\Authentication\Logout;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Akuikialie\Http\StatusCode;

class LogoutController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function destroy(Request $request)
    {
        try {
            auth()->logout();

            return redirect()->route('authentication.login');
        } catch (\Throwable $th) {
            return redirect()->route('authentication.login');
        }
    }
}
