<?php

namespace App\Http\Controllers\Authentication;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\User;
use Validator;
use Auth;

class LoginStoreController extends Controller
{
    public function index(Request $request)
    {
        try {
            $this->validate($request, [
                'username' => 'required',
                'password' => 'required',
            ]);

            if (Auth::attempt([
                'name' => $request->username,
                'password' => $request->password
            ])) {
                return redirect()->route('dashboard.index');
            } else {
                return back()->with('error', 'Email-Address And Password Are Wrong.');
            }
        } catch (\Throwable $th) {
            return response()->json([
                'error' => $th->getMessage(),
            ], 400);
        }
    }
}
