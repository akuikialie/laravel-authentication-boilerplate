<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('sign-in');
});


Route::group(['prefix' => 'authentication'], function () {
    Route::get('login', 'Authentication\LoginController@index')->name('authentication.login');
    Route::post('login', 'Authentication\LoginStoreController@index')->name('authentication.login.store');
    Route::get('logout', 'Authentication\LogoutDestroyController@index')->name('authentication.logout');
});
Route::get('dashboard', 'Dashboard\IndexController@index')->name('dashboard.index');
Route::group(['prefix' => 'user'], function () {
    Route::get('', 'User\AllController@index')->name('user.index');
    // Route::post('login', 'Authentication\LoginStoreController@index')->name('authentication.login.store');
});

Route::group(['prefix' => 'account'], function () {
    Route::get('profile', 'User\AllController@index')->name('account.profile');
    Route::get('setting', 'User\AllController@index')->name('account.setting');
    // Route::post('login', 'Authentication\LoginStoreController@index')->name('authentication.login.store');
});

// Route::post('login', function () {
//     return view('auth.login');
// })
// ->name('login');